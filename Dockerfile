FROM busybox
COPY nginx.conf.sigil .
ENTRYPOINT ["tail", "-f", "/dev/null"]