## Deploy

```
dokku config:set micro-chat-router \
    APP_BACK=micro-chat-back-5000 \
    APP_ADMIN=micro-chat-admin-5000 \
    APP_CLIENT=micro-chat-client-5000 \
    ADMIN_SERVER_NAME=admin.chatt.wolfsandbox.xyz \
    CLIENT_ROOT_HOST=client.chatt.wolfsandbox.xyz \

dokku domains:set micro-chat-router *.chatt.wolfsandbox.xyz

```
